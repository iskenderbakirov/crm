export default function({ store, redirect }) {
    if (store.getters['auth/getToken']) {
        return redirect('/dashboard')
    }
}
