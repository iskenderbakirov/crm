import pkg from './package'

export default {
    mode: 'spa',

    /*
     ** Headers of the page
     */
    head: {
        title: pkg.name,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'stylesheet',
                href:
                    'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic'
            },
            {
                rel: 'stylesheet',
                href:
                    'https://fonts.googleapis.com/icon?family=Material+Icons'
            }
        ]
    },

    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },

    /*
     ** Global CSS
     */
    css: ['~assets/global.sass'],

    styleResources: {
        // your settings here
        sass: [
            './assets/abstracts/_flex.sass',
            './assets/abstracts/_gridSettings.sass',
            './assets/abstracts/_imageSettings.sass',
            './assets/abstracts/_transition.sass',
            './assets/abstracts/_borderRadius.sass',
            './assets/abstracts/_btnSettings.sass',
            './assets/abstracts/_padding.sass',
            './assets/abstracts/_margin.sass',
            './assets/abstracts/_shadow.sass',
            './assets/vars/colors.sass',
            './assets/vars/button.sass',
            './assets/vars/form.sass'

        ]
    },

    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        {
            src: '~/plugins/api.js',
            ssr: false
        }
    ],

    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/style-resources'
    ],
    /*
     ** Axios module configuration
     */
    axios: {
        baseURL: 'http://vrs.sunrisetest.site/api'
    },

    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
            // Run ESLint on save
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    exclude: /(node_modules)/
                })
            }
        }
    }
}
