const Cookie = require('js-cookie')

export const state = () => ({
    token: null
})

export const getters = {
    getToken(state) {
        if (state.token !== null) {
            return state.token
        } else {
            let token = Cookie.get('token')
            return token
        }
    }
}

export const mutations = {
    setToken(state, token) {
        state.token = token
    }
}

export const actions = {
    async login({ commit, dispatch }, { username, password }) {
        const result = await this.$api('post', '/rest-auth/login/', {
            username: username,
            password: password
        }, 'application/json', {})

        if (result['success']) {
            let response = result['response']
            const token = response['key']

            dispatch('setTokenInCookie', { token: token })
        }

        return result
    },
    logout({ commit }) {
        Cookie.remove('token')

        commit('setToken', null)
    },
    setTokenInCookie({ commit }, { token }) {
        Cookie.set('token', token, {expires: 365})
        commit('setToken', token)
    }
}
